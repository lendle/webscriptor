/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package webscriptor;

import com.gargoylesoftware.htmlunit.ScriptResult;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.DomElement;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import java.io.File;
import java.net.URL;
import org.springframework.util.AntPathMatcher;
import webscriptor.factory.ApplicationConfigFactory;
import webscriptor.model.config.ActionConfig;
import webscriptor.model.config.ApplicationConfig;
import webscriptor.model.config.JqueryAttrPropertyConfig;
import webscriptor.model.config.PropertyConfig;
import webscriptor.model.config.ScriptActionConfig;
import webscriptor.model.config.ScriptorConfig;

/**
 *
 * @author lendle
 */
public class Tester {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
        // TODO code application logic here
        ApplicationConfigFactory factory=new ApplicationConfigFactory();
        ApplicationConfig app=factory.parse(new File("newXMLDocument1.xml"));
        URL url=new URL("http://www.nhu.edu.tw");
        AntPathMatcher matcher=new AntPathMatcher();
        for(ScriptorConfig scriptorConfig : app.getScriptors()){
            if(matcher.match(scriptorConfig.getUrlPattern(), url.toString())){
                WebClient webClient = new WebClient();
                HtmlPage page=webClient.getPage(url);
                if(scriptorConfig.getImportList().isEmpty()==false){
                    for(String importString : scriptorConfig.getImportList()){
                        DomElement domElement=page.createElement("script");
                        domElement.setAttribute("src", new File(scriptorConfig.getBaseDir(), importString).toURI().toURL().toString());
                        page.appendChild(domElement);
                    }
                }
                if(scriptorConfig.getInitScript()!=null){
                    ScriptResult result=page.executeJavaScript(scriptorConfig.getInitScript());
                    System.out.println(result);
                }
                for(PropertyConfig prop : scriptorConfig.getProperties()){
                    if(prop instanceof JqueryAttrPropertyConfig){
                        JqueryAttrPropertyConfig attrProp=(JqueryAttrPropertyConfig) prop;
                        String script="$(\""+attrProp.getSelector()+"\").attr(\""+attrProp.getAttrName()+"\");";
                        ScriptResult result=page.executeJavaScript(script);
                        System.out.println(result);
                    }
                }
                for(ActionConfig action : scriptorConfig.getActions()){
                    if(action instanceof ScriptActionConfig){
                        ScriptActionConfig script=(ScriptActionConfig) action;
                        String str="function "+script.getName()+"("+String.join(",", script.getArgs())+"){"+script.getScript()+"}";
                        page.executeJavaScript(str);
                        System.out.println(page.executeJavaScript("abc(3, 5);"));
                    }
                }
            }
        }
    }
    
}
