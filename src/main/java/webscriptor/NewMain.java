/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package webscriptor;

import java.io.File;
import java.net.URL;
import webscriptor.factory.ApplicationFactory;
import webscriptor.runtime.Application;

/**
 *
 * @author lendle
 */
public class NewMain {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception{
        // TODO code application logic here
        ApplicationFactory factory=new ApplicationFactory();
        Application application=factory.createApplication(new File("GoogleSearchApplication.xml"));
        application.gotoURL(new URL("http://www.google.com/"));

        System.out.println(application.executeAction("query", new String[]{"定義 "
                + "函數"} ));
        Thread.sleep(3000);
        System.out.println(application.executeAction("getResult", null));
    }
    
}
