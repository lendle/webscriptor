/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package webscriptor.utils;

/**
 *
 * @author lendle
 */
public class TypeConverter {
    public static String toString(Object result){
        return ""+result;
    }
    
    public static Integer toInt(Object result){
        return Integer.valueOf(""+result);
    }
    
    public static Long toLong(Object result){
        return Long.valueOf(""+result);
    }
    
    public static Float toFloat(Object result){
        return Float.valueOf(""+result);
    }
    
    public static Double toDouble(Object result){
        return Double.valueOf(""+result);
    }
    
    public static Object toType(String typeName, Object result){
        if(typeName.equals("String")){
            return toString(result);
        }else if(typeName.equals("int")){
            return toInt(result);
        }else if(typeName.equals("boolean")){
            return toBoolean(result);
        }else if(typeName.equals("long")){
            return toLong(result);
        }else if(typeName.equals("float")){
            return toFloat(result);
        }else if(typeName.equals("double")){
            return toDouble(result);
        }else{
            return null;
        }
    }

    private static Boolean toBoolean(Object result) {
         return Boolean.valueOf(""+result);
    }
}
