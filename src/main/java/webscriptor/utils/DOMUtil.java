/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package webscriptor.utils;

import java.util.ArrayList;
import java.util.List;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

/**
 *
 * @author lendle
 */
public class DOMUtil {
    public static Element getFirstElementChild(Node node) throws Exception{
        Node child=node.getFirstChild();
        while(child!=null){
            if(child.getNodeType()==Node.ELEMENT_NODE){
                return (Element) child;
            }
            child=child.getNextSibling();
        }
        return null;
    }
    
    public static List<Element> getElementChild(Node node, String tagName) throws Exception{
        List<Element> list=new ArrayList<>();
        Node child=node.getFirstChild();
        while(child!=null){
            if(child.getNodeType()==Node.ELEMENT_NODE && (tagName==null || child.getLocalName().equals(tagName))){
                list.add((Element)child);
            }
            child=child.getNextSibling();
        }
        return list;
    }
}
