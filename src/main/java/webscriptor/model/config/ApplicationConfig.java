/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package webscriptor.model.config;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author lendle
 */
public class ApplicationConfig {
    private List<ScriptorConfig> scriptors=new ArrayList<>();

    public List<ScriptorConfig> getScriptors() {
        return scriptors;
    }

    public void setScriptors(List<ScriptorConfig> scriptors) {
        this.scriptors = scriptors;
    }
    
    
}
