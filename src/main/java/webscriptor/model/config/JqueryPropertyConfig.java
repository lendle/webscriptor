/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package webscriptor.model.config;

/**
 *
 * @author lendle
 */
public abstract class JqueryPropertyConfig extends PropertyConfig{
    private String selector=null;

    public String getSelector() {
        return selector;
    }

    public void setSelector(String selector) {
        this.selector = selector;
    }
    
}
