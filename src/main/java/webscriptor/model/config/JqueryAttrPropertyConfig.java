/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package webscriptor.model.config;

/**
 *
 * @author lendle
 */
public class JqueryAttrPropertyConfig extends JqueryPropertyConfig{
    private String attrName=null;

    public String getAttrName() {
        return attrName;
    }

    public void setAttrName(String attrName) {
        this.attrName = attrName;
    }
    
}
