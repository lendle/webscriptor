/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package webscriptor.runtime;

import java.net.URL;
import webscriptor.model.config.ApplicationConfig;

/**
 *
 * @author lendle
 */
public interface Application extends ScriptRunnable{
    public void gotoURL(URL url) throws Exception;
    public Scriptor getCurrentScriptor();
    public ApplicationConfig getApplicationConfig();
}
