/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package webscriptor.runtime;

/**
 *
 * @author lendle
 */
public interface ScriptRunnable {
    public Object getProperty(String propertyName);
    public void setProperty(String propertyName, Object propertyValue);
    public Object executeAction(String name, Object[] args) throws Exception;
}
