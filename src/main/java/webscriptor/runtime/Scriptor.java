/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package webscriptor.runtime;

import webscriptor.model.config.ScriptorConfig;

/**
 *
 * @author lendle
 */
public interface Scriptor extends ScriptRunnable{
    public ScriptorConfig getScriptorConfig();
}
