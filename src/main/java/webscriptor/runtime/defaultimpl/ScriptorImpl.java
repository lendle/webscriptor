/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package webscriptor.runtime.defaultimpl;

import com.gargoylesoftware.htmlunit.ScriptResult;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.DomElement;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.javascript.host.WindowProxy;
import com.google.gson.Gson;
import java.io.File;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import net.sourceforge.htmlunit.corejs.javascript.NativeArray;
import net.sourceforge.htmlunit.corejs.javascript.NativeObject;
import webscriptor.factory.ApplicationConfigFactory;
import webscriptor.model.config.ActionConfig;
import webscriptor.model.config.ApplicationConfig;
import webscriptor.model.config.JqueryAttrPropertyConfig;
import webscriptor.model.config.JqueryPropertyConfig;
import webscriptor.model.config.JqueryTextPropertyConfig;
import webscriptor.model.config.JqueryValuePropertyConfig;
import webscriptor.model.config.PropertyConfig;
import webscriptor.model.config.ScriptActionConfig;
import webscriptor.model.config.ScriptorConfig;
import webscriptor.runtime.Scriptor;
import webscriptor.utils.TypeConverter;

/**
 *
 * @author lendle
 */
public class ScriptorImpl implements Scriptor {

    private ScriptorConfig config = null;
    private HtmlPage page = null;
    private Map<String, PropertyConfig> propConfigs = new HashMap<>();
    private Map<String, ActionConfig> actionConfigs = new HashMap<>();

    public ScriptorImpl(HtmlPage page, ScriptorConfig config) throws Exception {
        this.page = page;
        this.config = config;
        DomElement domElement = page.createElement("script");
        domElement.setAttribute("src", "https://code.jquery.com/jquery-1.12.3.min.js");
        page.appendChild(domElement);

        for (ActionConfig action : config.getActions()) {
            if (action instanceof ScriptActionConfig) {
                ScriptActionConfig scriptAction = (ScriptActionConfig) action;
                String str = "function " + scriptAction.getName() + "(" + String.join(",", scriptAction.getArgs()) + "){" + scriptAction.getScript() + "}";
                page.executeJavaScript(str);
                actionConfigs.put(scriptAction.getName(), action);
            }
        }

        for (PropertyConfig prop : config.getProperties()) {
            if (prop instanceof JqueryPropertyConfig) {
                JqueryPropertyConfig jqueryProp = (JqueryPropertyConfig) prop;
                String getScript = "function __get_" + prop.getName() + "__" + "(){";
                String setScript = "function __set_" + prop.getName() + "__(value){";
                if (jqueryProp instanceof JqueryAttrPropertyConfig) {
                    JqueryAttrPropertyConfig jquery = (JqueryAttrPropertyConfig) jqueryProp;
                    getScript += "return $(\"" + jqueryProp.getSelector() + "\").attr(\"" + jquery.getAttrName() + "\");";
                    setScript += "$(\"" + jqueryProp.getSelector() + "\").attr(\"" + jquery.getAttrName() + "\", value);";
                } else if (jqueryProp instanceof JqueryValuePropertyConfig) {
                    JqueryValuePropertyConfig jquery = (JqueryValuePropertyConfig) jqueryProp;
                    getScript += "return $(\"" + jqueryProp.getSelector() + "\").val();";
                    setScript += "$(\"" + jqueryProp.getSelector() + "\").val(value);";
                } else if (jqueryProp instanceof JqueryTextPropertyConfig) {
                    JqueryTextPropertyConfig jquery = (JqueryTextPropertyConfig) jqueryProp;
                    getScript += "return $(\"" + jqueryProp.getSelector() + "\").text();";
                    setScript += "$(\"" + jqueryProp.getSelector() + "\").text(value);";
                }
                propConfigs.put(prop.getName(), prop);
                getScript += "}";
                setScript += "}";
                page.executeJavaScript(getScript);
                page.executeJavaScript(setScript);
            }
        }

        if (config.getInitScript() != null) {
            page.executeJavaScript(config.getInitScript());
        }
        //WindowProxy window=(WindowProxy) page.executeJavaScript("window").getJavaScriptResult();
        /*DomNodeList<DomElement> forms=page.getElementsByTagName("form");
        for(int i=0; forms!=null && i<forms.getLength(); i++){
            HtmlForm form=(HtmlForm) forms.get(i);
            form.
        }*/
    }

    @Override
    public Object getProperty(String propertyName) {
        PropertyConfig config = this.propConfigs.get(propertyName);
        ScriptResult result = this.page.executeJavaScript("__get_" + propertyName + "__();");
        return TypeConverter.toType(config.getReturnType(), result.getJavaScriptResult());
    }

    @Override
    public void setProperty(String propertyName, Object propertyValue) {
        PropertyConfig config = this.propConfigs.get(propertyName);
        String value = "" + propertyValue;
        if (config.getReturnType().equals("String")) {
            value = "\"" + value + "\"";
        }
        this.page.executeJavaScript("__set_" + propertyName + "__(" + value + ");");
    }

    @Override
    public Object executeAction(String name, Object[] args) throws Exception {
        ScriptResult result = null;
        if (args != null) {
            String[] stringArgs = new String[args.length];
            int index = 0;
            if (args.length > 0) {
                for (Object arg : args) {
                    if (arg instanceof String) {
                        stringArgs[index++] = "\"" + arg + "\"";
                    } else {
                        stringArgs[index++] = "" + arg;
                    }
                }
            }
            result = this.page.executeJavaScript(name + "(" + String.join(",", stringArgs) + ");");
        } else {
            result = this.page.executeJavaScript(name + "();");
        }
        ActionConfig config = this.actionConfigs.get(name);
        Object objRet = result.getJavaScriptResult();
        //System.out.println("test3: "+page.asXml());
        if ((objRet instanceof NativeArray) && config.getReturnType().equals("String")) {
            //System.out.println(objRet);
            //System.out.println("1:"+ Arrays.deepToString(((NativeArray)objRet).getAllIds()));
            return new Gson().toJson(((NativeArray) objRet).toArray());
        } else if ((objRet instanceof NativeObject) && config.getReturnType().equals("String")) {
            //System.out.println(objRet);
            //System.out.println("1:"+ Arrays.deepToString(((NativeArray)objRet).getAllIds()));
            return new Gson().toJson(((NativeObject) objRet));
        }
        return TypeConverter.toType(config.getReturnType(), objRet);
    }

    public static void main(String[] args) throws Exception {
        ApplicationConfigFactory factory = new ApplicationConfigFactory();
        ApplicationConfig app = factory.parse(new File("newXMLDocument1.xml"));
        URL url = new URL("http://www.nhu.edu.tw");
        ScriptorConfig scriptorConfig = app.getScriptors().get(0);
        WebClient webClient = new WebClient();
        HtmlPage page = webClient.getPage(url);
        ScriptorImpl impl = new ScriptorImpl(page, scriptorConfig);
        System.out.println(impl.getProperty("prop2"));
        System.out.println(impl.executeAction("abc", new Object[]{1, 2}));
    }

    @Override
    public ScriptorConfig getScriptorConfig() {
        return this.config;
    }

}
