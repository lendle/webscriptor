/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package webscriptor.runtime.defaultimpl;

import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.NicelyResynchronizingAjaxController;
import com.gargoylesoftware.htmlunit.ScriptException;
import com.gargoylesoftware.htmlunit.SilentCssErrorHandler;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.WebWindowEvent;
import com.gargoylesoftware.htmlunit.WebWindowListener;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.javascript.JavaScriptEngine;
import com.gargoylesoftware.htmlunit.javascript.JavaScriptErrorListener;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.util.AntPathMatcher;
import webscriptor.model.config.ApplicationConfig;
import webscriptor.model.config.ScriptorConfig;
import webscriptor.runtime.Application;
import webscriptor.runtime.Scriptor;

/**
 *
 * @author lendle
 */
public class ApplicationImpl implements Application {
    
    private ApplicationConfig config = null;
    private URL currentURL = null;
    private WebClient webClient = null;
    private HtmlPage currentPage = null;
    
    public ApplicationImpl(ApplicationConfig config) {
        this.config = config;
        this.webClient = new WebClient();
        this.webClient.setCssErrorHandler(new SilentCssErrorHandler());
        
        this.webClient.setJavaScriptErrorListener(new JavaScriptErrorListener(){

            @Override
            public void scriptException(HtmlPage hp, ScriptException se) {
            }

            @Override
            public void timeoutError(HtmlPage hp, long l, long l1) {
            }

            @Override
            public void malformedScriptURL(HtmlPage hp, String string, MalformedURLException murle) {
            }

            @Override
            public void loadScriptError(HtmlPage hp, URL url, Exception excptn) {
            }
        });
        //java.util.logging.Logger.getLogger("com.gargoylesoftware").setLevel(Level.OFF); 
        this.webClient.setAjaxController(new NicelyResynchronizingAjaxController());
//        this.webClient.addWebWindowListener(new WebWindowListener(){
//
//            @Override
//            public void webWindowOpened(WebWindowEvent wwe) {
//                System.out.println("1:"+wwe.getNewPage().getUrl());
//            }
//
//            @Override
//            public void webWindowContentChanged(WebWindowEvent wwe) {
//                System.out.println("2:"+wwe.getNewPage().getUrl());
//            }
//
//            @Override
//            public void webWindowClosed(WebWindowEvent wwe) {
//                System.out.println("3:"+wwe.getNewPage().getUrl());
//            }
//        });
    }
    
    @Override
    public void gotoURL(URL url) throws Exception {
        //System.out.println("gotoURL: " + url);
        this.currentPage = this.webClient.getPage(url);
        //this.webClient.
        this.currentURL = url;
        this.webClient.addWebWindowListener(new WebWindowListener() {
            
            @Override
            public void webWindowOpened(WebWindowEvent wwe) {
                //System.out.println("webWindowOpened: " + wwe);
            }
            
            @Override
            public void webWindowContentChanged(WebWindowEvent wwe) {
                try {
                    //System.out.println("webWindowContentChanged: "+wwe.getNewPage().getUrl());
                    if (currentURL.equals(wwe.getNewPage().getUrl()) == false && wwe.getNewPage().getUrl().toString().equals("about:blank")==false) {
                        currentURL = wwe.getNewPage().getUrl();
                        gotoURL(currentURL);
                    }
                } catch (Exception ex) {
                    Logger.getLogger(ApplicationImpl.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            
            @Override
            public void webWindowClosed(WebWindowEvent wwe) {
                //System.out.println("webWindowClosed: " + wwe.getNewPage().getUrl());
            }
        });
    }
    
    public WebClient getWebClient() {
        return webClient;
    }
    
    @Override
    public Scriptor getCurrentScriptor() {
        //System.out.println("currentURL=" + this.currentURL);
        if (this.currentURL == null) {
            return null;
        }
        for (ScriptorConfig scriptorConfig : this.config.getScriptors()) {
            AntPathMatcher matcher = new AntPathMatcher();
            if (matcher.match(scriptorConfig.getUrlPattern(), this.currentURL.toString())) {
                try {
                    ScriptorImpl scriptor = new ScriptorImpl(this.currentPage, scriptorConfig);
                    return scriptor;
                } catch (Exception ex) {
                    Logger.getLogger(ApplicationImpl.class.getName()).log(Level.SEVERE, null, ex);
                    return null;
                }
            } else {
                System.out.println(scriptorConfig.getUrlPattern() + ":" + matcher.match(scriptorConfig.getUrlPattern(), this.currentURL.toString()) + ", " + this.currentURL.toString());
            }
        }
        return null;
    }
    
    @Override
    public Object getProperty(String propertyName) {
        return this.getCurrentScriptor().getProperty(propertyName);
    }
    
    @Override
    public void setProperty(String propertyName, Object propertyValue) {
        this.getCurrentScriptor().setProperty(propertyName, propertyValue);
    }
    
    @Override
    public Object executeAction(String name, Object[] args) throws Exception {
        return this.getCurrentScriptor().executeAction(name, args);
    }
    
    @Override
    public ApplicationConfig getApplicationConfig() {
        return this.config;
    }
    
}
