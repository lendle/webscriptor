/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package webscriptor.factory;

import java.io.File;
import java.net.URL;
import webscriptor.model.config.ApplicationConfig;
import webscriptor.runtime.Application;
import webscriptor.runtime.Scriptor;
import webscriptor.runtime.defaultimpl.ApplicationImpl;

/**
 *
 * @author lendle
 */
public class ApplicationFactory {
    public Application createApplication(ApplicationConfig config){
        return new ApplicationImpl(config);
    }
 
    public Application createApplication(File configFile) throws Exception{
        return new ApplicationImpl(new ApplicationConfigFactory().parse(configFile));
    }
    
    public static void main(String [] args) throws Exception{
        ApplicationFactory factory=new ApplicationFactory();
        Application application=factory.createApplication(new File("newXMLDocument1.xml"));
//        application.gotoURL(new URL("http://www.nhu.edu.tw"));
//        Scriptor scriptor=application.getCurrentScriptor();
//        
//        System.out.println(scriptor.getProperty("prop2"));
//        System.out.println(scriptor.executeAction("abc", new Object[]{1, 2}));
//        System.out.println(scriptor.executeAction("abcd", null));
//        System.out.println(scriptor.executeAction("abcde", null));
        application.gotoURL(new URL("http://www.google.com/"));
        Thread.sleep(30000);
        //System.out.println(((ApplicationImpl)application).getWebClient().getCurrentWindow().getEnclosedPage().getUrl());
        System.out.println(application.executeAction("query", null));
        //System.out.println(((ApplicationImpl)application).getWebClient().getCurrentWindow().getEnclosedPage().getUrl());
        Thread.sleep(30000);
        //System.out.println(((ApplicationImpl)application).getWebClient().getCurrentWindow().getEnclosedPage().getUrl());
        //application.gotoURL(new URL("http://www.google.com.tw/search?hl=zh-TW&source=hp&biw=&bih=&q=apple&gbv=2&oq=&gs_l="));
        System.out.println(application.executeAction("getResult", null));
    }
}
