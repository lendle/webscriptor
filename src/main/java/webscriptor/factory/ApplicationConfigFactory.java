/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package webscriptor.factory;

import com.google.gson.Gson;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.List;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import webscriptor.model.config.ApplicationConfig;
import webscriptor.model.config.JqueryAttrPropertyConfig;
import webscriptor.model.config.JqueryTextPropertyConfig;
import webscriptor.model.config.JqueryValuePropertyConfig;
import webscriptor.model.config.PropertyConfig;
import webscriptor.model.config.ScriptActionConfig;
import webscriptor.model.config.ScriptorConfig;
import webscriptor.utils.DOMUtil;

/**
 *
 * @author lendle
 */
public class ApplicationConfigFactory {

    public ApplicationConfig parse(File configFile) throws Exception {
        try (Reader input = new InputStreamReader(new FileInputStream(configFile), "utf-8")) {
            Document appDOM = this.loadApplicationConfigDOM(input);
            ApplicationConfig appConfig = new ApplicationConfig();
            NodeList nodeList = appDOM.getDocumentElement().getElementsByTagNameNS("*","scriptor");
            for (int i = 0; i < nodeList.getLength(); i++) {
                Element scriptorElement = (Element) nodeList.item(i);
                String pattern = scriptorElement.getAttribute("url-pattern");
                String file = scriptorElement.getAttribute("file");
                File fileObj = new File(file);
                if (!fileObj.exists() || !fileObj.isFile()) {
                    fileObj = new File(configFile.getParentFile(), file);
                }
                try (Reader scrReader = new InputStreamReader(new FileInputStream(fileObj), "utf-8")) {
                    Document scrDOM = this.loadConfigDOM(scrReader);
                    ScriptorConfig scriptorConfig = this.parseScriptorConfig(scrDOM, configFile.getParentFile());
                    scriptorConfig.setUrlPattern(pattern);
                    appConfig.getScriptors().add(scriptorConfig);
                }
            }
            return appConfig;
        }
    }

    /*public ApplicationConfig parse(Reader input, File baseDir) throws Exception {
        Document appDOM = this.loadApplicationConfigDOM(input);
            ApplicationConfig appConfig = new ApplicationConfig();
            NodeList nodeList = appDOM.getElementsByTagName("scriptor");
            for (int i = 0; i < nodeList.getLength(); i++) {
                Element scriptorElement = (Element) nodeList.item(i);
                String pattern = scriptorElement.getAttribute("url-pattern");
                String file = scriptorElement.getAttribute("file");
                File fileObj = new File(baseDir, file);
                try (Reader scrReader = new InputStreamReader(new FileInputStream(fileObj), "utf-8")) {
                    Document scrDOM = this.loadConfigDOM(scrReader);
                    ScriptorConfig scriptorConfig = this.parseScriptorConfig(scrDOM);
                    scriptorConfig.setUrlPattern(pattern);
                    appConfig.getScriptors().add(scriptorConfig);
                }
            }
            return appConfig;
    }*/
    
    protected ScriptorConfig parseScriptorConfig(Document scr, File baseDir) throws Exception {
        ScriptorConfig config = new ScriptorConfig();
        config.setBaseDir(baseDir);
        NodeList list = scr.getElementsByTagNameNS("*" ,"import");
        for (int i = 0; list != null && i < list.getLength(); i++) {
            Element element = (Element) list.item(i);
            config.getImportList().add(element.getAttribute("file"));
        }
        list = scr.getElementsByTagNameNS("*" ,"init");
        if (list != null && list.getLength() >= 1) {
            Element element = DOMUtil.getFirstElementChild(((Element) list.item(0)));
            config.setInitScript(element.getTextContent().trim());
        }
        list = scr.getElementsByTagNameNS("*" ,"properties");
        if (list != null && list.getLength() >= 1) {
            Element element = (Element) list.item(0);
            NodeList propList = element.getChildNodes();
            for (int i = 0; propList != null && i < propList.getLength(); i++) {
                if (propList.item(i).getNodeType() == Node.ELEMENT_NODE) {
                    Element prop = (Element) propList.item(i);
                    //System.out.println(prop.getNodeName());
                    if (prop.getLocalName().equals("jqueryTextProperty")) {
                        config.getProperties().add(this.parseJqueryTextProperty(prop));
                    }else if (prop.getLocalName().equals("jqueryValueProperty")) {
                        config.getProperties().add(this.parseJqueryValueProperty(prop));
                    }else if (prop.getLocalName().equals("jqueryAttrProperty")) {
                        config.getProperties().add(this.parseJqueryAttrProperty(prop));
                    }
                }
            }
        }
        
        list = scr.getElementsByTagNameNS("*" ,"actions");
        if (list != null && list.getLength() >= 1) {
            Element element = (Element) list.item(0);
            List<Element> scriptElements=DOMUtil.getElementChild(element, "scriptAction");
            if(scriptElements!=null){
                for(Element scriptElement : scriptElements){
                    ScriptActionConfig scriptConfig=new ScriptActionConfig();
                    scriptConfig.setReturnType(scriptElement.getAttributeNS("lendle.webscriptor", "returnType"));
                    String argsStr=scriptElement.getAttributeNS("lendle.webscriptor", "args");
                    if(argsStr!=null){
                        scriptConfig.setArgs(argsStr.split("(\\s)+"));
                    }
                    scriptConfig.setScript(DOMUtil.getFirstElementChild(scriptElement).getTextContent().trim());
                    config.getActions().add(scriptConfig);
                    if(scriptElement.getAttributeNS("lendle.webscriptor", "name")!=null){
                        scriptConfig.setName(scriptElement.getAttributeNS("lendle.webscriptor", "name"));
                    }
                }
            }
        }
        return config;
    }

    protected PropertyConfig parseJqueryTextProperty(Element prop) throws Exception {
        JqueryTextPropertyConfig propConfig = new JqueryTextPropertyConfig();
        propConfig.setName(prop.getAttributeNS("lendle.webscriptor", "name"));
        propConfig.setReturnType(prop.getAttributeNS("lendle.webscriptor", "returnType"));
        Element selector=DOMUtil.getFirstElementChild(prop);
        propConfig.setSelector(this.parseSelector(selector));
        
        return propConfig;
    }
    
    protected PropertyConfig parseJqueryValueProperty(Element prop) throws Exception {
        JqueryValuePropertyConfig propConfig = new JqueryValuePropertyConfig();
        propConfig.setName(prop.getAttributeNS("lendle.webscriptor", "name"));
        propConfig.setReturnType(prop.getAttributeNS("lendle.webscriptor", "returnType"));
        Element selector=DOMUtil.getFirstElementChild(prop);
        propConfig.setSelector(this.parseSelector(selector));
        
        return propConfig;
    }

    protected String parseSelector(Element selector) throws Exception{
        Element firstChild=DOMUtil.getFirstElementChild(selector);
        if(firstChild.getLocalName().equals("id")){
            return "#"+firstChild.getTextContent();
        }else if(firstChild.getLocalName().equals("tag")){
            return firstChild.getTextContent();
        }else if(firstChild.getLocalName().equals("class")){
            return "."+firstChild.getTextContent();
        }else{
            return null;
        }
    }
    
    protected Document loadApplicationConfigDOM(Reader input) throws Exception {
        return this.loadConfigDOM(input);
    }

    protected Document loadScriptorConfigDOM(Reader input) throws Exception {
        return this.loadConfigDOM(input);
    }

    protected Document loadConfigDOM(Reader input) throws Exception {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        dbf.setNamespaceAware(true);
        DocumentBuilder db = dbf.newDocumentBuilder();
        Document doc = db.parse(new InputSource(input));
        return doc;
    }

    protected PropertyConfig parseJqueryAttrProperty(Element prop) throws Exception{
        JqueryAttrPropertyConfig propConfig = new JqueryAttrPropertyConfig();
        propConfig.setName(prop.getAttributeNS("lendle.webscriptor", "name"));
        propConfig.setReturnType(prop.getAttributeNS("lendle.webscriptor","returnType"));
        Element selector=DOMUtil.getFirstElementChild(prop);
        propConfig.setSelector(this.parseSelector(selector));
        propConfig.setAttrName(prop.getAttributeNS("lendle.webscriptor","attrName"));
        
        return propConfig;
    }
    
    public static void main(String [] args) throws Exception{
        ApplicationConfigFactory factory=new ApplicationConfigFactory();
        ApplicationConfig app=factory.parse(new File("newXMLDocument1.xml"));
       
        ScriptorConfig scriptor=app.getScriptors().get(0);
        System.out.println(new Gson().toJson(scriptor));
        System.out.println( ((ScriptActionConfig)scriptor.getActions().get(0)).getScript());
    }
}
